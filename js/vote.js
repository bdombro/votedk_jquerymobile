STATE = {
    tRound: 1,
    oProgress: {1: false, 2: false, 3: false, 4: false, 5: false, 6: false}
}


$(function() {
    $("#pageTitle").html('Round '+STATE.tRound)
    $( "#progressbar" ).progressbar( { value: 0 });
    $('[data-role="panel"]').swiperight(function() { $(this).find('a[href$=#vote]').click(); });
    $.event.special.swipe.horizontalDistanceThreshold = 230;
    $.event.special.swipe.verticalDistanceThreshold = 400;
 
 });
 
function fCheckIfVoteComplete() {
    for(var i in STATE.oProgress) {
        if(STATE.oProgress[i]===false) {
            console.log(i+':'+STATE.oProgress[i]);
            return false;
        }
    }
    return true;
}

function fSaveVote(tVoteNumber) {
    if(STATE.oProgress[tVoteNumber]===false) {
        STATE.oProgress[tVoteNumber]=true;
        tProgress=$( "#progressbar" ).progressbar( "option", "value");
        $( "#progressbar" ).progressbar( "option", "value",  tProgress+100/6); //incriment the progress bar
        $('[data-role="listview"] li:nth-child('+tVoteNumber+')').css('background','#d8c7c7');
    }
    if(fCheckIfVoteComplete()) {
        $("#submit-block-button").hide();
        $("#submit-button").show();
    }
    
}

function fSubmitVote() {
    STATE.tRound+=1;
    alert('Thanks! On to Round '+STATE.tRound);
    
    //Reset everything
    STATE.oProgress = {1: false, 2: false, 3: false, 4: false, 5: false, 6: false};
    $( "#progressbar" ).progressbar( "option", "value",  0);
    $("#submit-block-button").show();
    $("#submit-button").hide();
    $("#sing-it-slider-11").val(5).slider("refresh");
    $("#sing-it-slider-12").val(5).slider("refresh");
    $("#sing-it-slider-21").val(5).slider("refresh");
    $("#sing-it-slider-22").val(5).slider("refresh");
    $("#sing-it-slider-31").val(5).slider("refresh");
    $("#sing-it-slider-32").val(5).slider("refresh");
    $("#sing-it-slider-41").val(5).slider("refresh");
    $("#sing-it-slider-42").val(5).slider("refresh");
    $("#sing-it-slider-51").val(5).slider("refresh");
    $("#sing-it-slider-52").val(5).slider("refresh");
    $("#sing-it-slider-61").val(5).slider("refresh");
    $("#sing-it-slider-62").val(5).slider("refresh");
    $('[data-role="listview"] li').css('background','');
    
    
    //Now Start Next Round
    $("#pageTitle").html('Round '+STATE.tRound)
}
